class AffinitiesController < PropertiesController

  def index
    @properties = Affinity.paginate(page: params[:page])
  end

  def new
    @property = Affinity.new
  end
  
  def create
    @property = Affinity.new(action_params)
    super
  end 

  private

    def action_params
      params.require(:affinity).permit(:name, :description)
    end  

    def set_property_type
      @property_type = "organ/tissue affinity"
    end
end
