class HerbsController < ApplicationController
  before_action :logged_in_user, only: [ :new, :create, :edit, :update, :destroy ]
  before_action :user_can_edit,  only: [ :new, :create, :edit, :update, :destroy ]

  def index
    @herbs = Herb.paginate(page: params[:page])

  end

  def new
    @herb = Herb.new
  end

  def create
    @herb = Herb.new(herb_params)
    if @herb.save
      flash[:success] = "Successfully added \'#{@herb.name}\'."
      redirect_to @herb
    else
      render 'new'
    end
  end

  def show
    @herb = Herb.find(params[:id])    
  end

  def edit
    @herb = Herb.find(params[:id])
  end

  def update
    @herb = Herb.find(params[:id])
    if @herb.update_attributes(herb_params)
      flash[:success] = 'Successfully updated herb'
      redirect_to @herb
    else
      render 'edit'
    end
  end

  def destroy
    @herb = Herb.find(params[:id])    
    name = @herb.name
    @herb.destroy
    flash[:success] = "Herb '#{name}' deleted"
    redirect_to herbs_url
  end

  private

  def herb_params
    params.require(:herb).permit(:name, :latin, :warming, :cooling, :drying,
                                 :moistening, :toning, :relaxing)
  end
end
