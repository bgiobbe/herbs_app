class FoundationalActionsController < PropertiesController

  def index
    @properties = FoundationalAction.paginate(page: params[:page])
  end

  def new
    @property = FoundationalAction.new
  end
  
  def create
    @property = FoundationalAction.new(action_params)
    super
  end 

  private

    def action_params
      params.require(:foundational_action).permit(:name, :description)
    end  

    def set_property_type
      @property_type = "foundational action"
    end
end
