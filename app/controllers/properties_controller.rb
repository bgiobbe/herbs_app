class PropertiesController < ApplicationController
  before_action :logged_in_user, only: [ :new, :create, :edit, :update, :destroy ]
  before_action :user_can_edit, only: [ :new, :create, :edit, :update, :destroy ]
  before_action :set_property_type, only: [ :create, :update, :destroy ]

  def create
    if @property.save
      flash[:success] = "Created #{@property_type} \"#{@property.name}\"."
      redirect_to @property
    else
      render 'new'
    end
  end

  def show
    @property = Property.find(params[:id])
  end

  def edit
    @property = Property.find(params[:id])
  end

  def update
    @property = Property.find(params[:id])    
    if @property.update_attributes(action_params)
      flash[:success] = "Updated #{@property_type} \"#{@property.name}\"."
      redirect_to @property
    else
      render 'edit'
    end
  end

  def destroy
    @property = Property.find(params[:id])    
    name = @property.name
    @property.destroy
    flash[:success] = "Deleted #{@property_type} \"#{name}\"."
    redirect_to action: :index
  end
end
