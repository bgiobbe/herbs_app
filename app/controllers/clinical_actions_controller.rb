class ClinicalActionsController < PropertiesController
  before_action :set_property_type, only: [ :create, :update, :destroy ]

  def index
    @properties = ClinicalAction.paginate(page: params[:page])
  end

  def new
    @property = ClinicalAction.new
  end
  
  def create
    @property = ClinicalAction.new(action_params)
    super
  end 

  private

    def action_params
      params.require(:clinical_action).permit(:name, :description)
    end  

    def set_property_type
      @property_type = "clinical action"
    end
end
