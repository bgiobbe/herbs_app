class Herb < ActiveRecord::Base
  default_scope -> { order(:name) }
  validates :name, presence: true, length: {maximum: 30},
                   uniqueness: { case_sensitive: false } 
  validates :latin, length: {maximum: 40}
  validates :warming, :cooling, :drying, :moistening, :toning, :relaxing,
            numericality: { only_integer: true,
                            greater_than_or_equal_to: 0,
                            less_than_or_equal_to: 3 }
end
