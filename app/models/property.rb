class Property < ActiveRecord::Base
  default_scope -> { order(:name) }
  before_save { self.name = name.downcase }
  validates :name,  presence: true, length: { maximum: 40 },
                    uniqueness: { case_sensitive: false }
  validates :type, presence: true
end
