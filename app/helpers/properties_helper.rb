module PropertiesHelper

  def display_type(property)
    { 'FoundationalAction' => 'Foundational (primary) action',
      'ClinicalAction'     => 'Clinical (secondary) action',
      'Affinity'           => 'Organ/tissue affinity'
    }.fetch(property.type)
  end
end
