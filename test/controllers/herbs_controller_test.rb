require 'test_helper'

class HerbsControllerTest < ActionController::TestCase

  def setup
    @unauthorized_user = users(:lana)
    @herb = herbs(:dandelion)
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get show" do
    get :show, id: @herb
    assert_response :success
  end

  test "should redirect new if not logged in" do
    get :new
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect create if not logged in" do
    assert_no_difference 'Herb.count' do
      post :create, herb: { name: 'lorem', latin: 'ipsum' }
    end
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect edit if not logged in" do
    get :edit, id: @herb
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update if not logged in" do
    patch :update, id: @herb, herb: { name: @herb.name,
                                      latin: @herb.latin }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect destroy if not logged in" do
    assert_no_difference 'Herb.count' do
      delete :destroy, id: @herb
    end
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect new if not editor" do
    log_in_as(@unauthorized_user)
    get :new
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect create if not editor" do
    log_in_as(@unauthorized_user)
    assert_no_difference 'Herb.count' do
      post :create, herb: { name: 'lorem', latin: 'ipsum' }
    end
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect edit if not editor" do
    log_in_as(@unauthorized_user)
    get :edit, id: @herb
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect update if not editor" do
    log_in_as(@unauthorized_user)
    patch :update, id: @herb, herb: { name: @herb.name,
                                      latin: @herb.latin }
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect destroy if not editor" do
    log_in_as(@unauthorized_user)
    assert_no_difference 'Herb.count' do
      delete :destroy, id: @herb
    end
    assert_not flash.empty?
    assert_redirected_to root_url
  end
end
