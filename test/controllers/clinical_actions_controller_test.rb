require 'test_helper'

class ClinicalActionsControllerTest < ActionController::TestCase

  def setup
    @unauthorized_user = users(:lana)
    @property = properties(:analgesic)
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get show" do
    get :show, id: @property
    assert_response :success
  end

  test "should redirect new if not logged in" do
    get :new
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect create if not logged in" do
    assert_no_difference 'ClinicalAction.count' do
      post :create, clinical_action: { name: 'lorem',
                                           description: 'ipsum' }
    end
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect edit if not logged in" do
    get :edit, id: @property
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update if not logged in" do
    patch :update, id: @property, clinical_action: {
                                    name: @property.name,
                                    description: @property.description }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect destroy if not logged in" do
    assert_no_difference 'ClinicalAction.count' do
      delete :destroy, id: @property
    end
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect new if not editor" do
    log_in_as(@unauthorized_user)
    get :new
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect create if not editor" do
    log_in_as(@unauthorized_user)
    assert_no_difference 'ClinicalAction.count' do
      post :create, clinical_action: { name: 'lorem',
                                           description: 'ipsum' }
    end
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect edit if not editor" do
    log_in_as(@unauthorized_user)
    get :edit, id: @property
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect update if not editor" do
    log_in_as(@unauthorized_user)
    patch :update, id: @property, clinical_action: {
                                    name: @property.name,
                                    description: @property.description }
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect destroy if not editor" do
    log_in_as(@unauthorized_user)
    assert_no_difference 'ClinicalAction.count' do
      delete :destroy, id: @property
    end
    assert_not flash.empty?
    assert_redirected_to root_url
  end
end
