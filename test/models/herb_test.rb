require 'test_helper'

class HerbTest < ActiveSupport::TestCase

  def setup
    @herb = herbs(:dandelion)
  end

  test "should be valid" do
    assert @herb.valid?
  end

  test "name should be present" do
    @herb.name = ''
    assert_not @herb.valid?
  end

  test "name should not be too long" do
    @herb.name = 'x' * 31
    assert_not @herb.valid?
  end

  test "name must be unique" do
    duplicate_herb = @herb.dup
    @herb.save
    assert_not duplicate_herb.valid?
  end

  test "latin name should not be too long" do
    @herb.latin = 'x' * 41
    assert_not @herb.valid?
  end

  test "warming should be between 0 and 3" do
    @herb.warming = -1
    assert_not @herb.valid?
    @herb.warming = 4
    assert_not @herb.valid?
  end

  test "cooling should be between 0 and 3" do
    @herb.cooling = -1
    assert_not @herb.valid?
    @herb.cooling = 4
    assert_not @herb.valid?
  end

  test "drying should be between 0 and 3" do
    @herb.drying = -1
    assert_not @herb.valid?
    @herb.drying = 4
    assert_not @herb.valid?
  end

  test "moistening should be between 0 and 3" do
    @herb.moistening = -1
    assert_not @herb.valid?
    @herb.moistening = 4
    assert_not @herb.valid?
  end

  test "toning should be between 0 and 3" do
    @herb.toning = -1
    assert_not @herb.valid?
    @herb.toning = 4
    assert_not @herb.valid?
  end

  test "relaxing should be between 0 and 3" do
    @herb.relaxing = -1
    assert_not @herb.valid?
    @herb.relaxing = 4
    assert_not @herb.valid?
  end
end
