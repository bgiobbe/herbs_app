require 'test_helper'

class PropertyTest < ActiveSupport::TestCase

  def setup
    @property = Property.new(name: "action", description: "does something", type: "FoundationalAction")
  end

  test "should be valid" do
    assert @property.valid?
  end

  test "name should be present" do
    @property.name = ""
    assert_not @property.valid?
  end

  test "name should not be too long" do
    @property.name = "x" * 41
    assert_not @property.valid?
  end

  test "type should be present" do
    @property.type = nil
    assert_not @property.valid?
  end

  test "name must be unique" do
    duplicate_property = @property.dup
    duplicate_property.name = @property.name.upcase
    @property.save
    assert_not duplicate_property.valid?
  end

  test "name should be saved as lowercase" do
    mixed_case_name = "MixedCaseName"
    @property.name = mixed_case_name
    @property.save
    assert_equal mixed_case_name.downcase, @property.reload.name
  end
end
