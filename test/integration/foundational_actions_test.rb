require 'test_helper'

class FoundationalActionsTest < ActionDispatch::IntegrationTest

  def setup
    @editor = users(:archer)
    @foundational_action = properties(:stimulant)
  end

  test "invalid info for new property" do
    log_in_as(@editor)
    get new_foundational_action_path
    assert_no_difference 'FoundationalAction.count' do
      post foundational_actions_path, foundational_action: {
                                        name:  "",
                                        description: "lorem ipsum" }
    end
    assert_template 'new'
  end

  test "create new foundational action with friendly forwarding" do
    get new_foundational_action_path
    assert_redirected_to login_url
    follow_redirect!
    log_in_as(@editor)
    assert_redirected_to new_foundational_action_url
    assert_difference 'FoundationalAction.count', 1 do
      post_via_redirect foundational_actions_path,
                        foundational_action: { name:  "action",
                                               description: "lorem ipsum" }
    end
    assert_template 'show'
  end

  test "unsuccessful edit" do
    log_in_as(@editor)
    get edit_foundational_action_path(@foundational_action)
    assert_template 'edit'
    patch foundational_action_path(@foundational_action),
          foundational_action: { name: "", description: "lorem ipsum" }
    assert_template 'edit'
  end

  test "successful edit with friendly forwarding" do
    get edit_foundational_action_path(@foundational_action)
    assert_redirected_to login_url
    follow_redirect!
    log_in_as(@editor)
    assert_redirected_to edit_foundational_action_url(@foundational_action)
    follow_redirect!
    assert_template 'edit'
    name = "xyzzy"
    description = "lorem ipsum"
    patch foundational_action_path(@foundational_action),
          foundational_action: { name: name,
                                 description: description }
    assert_not flash.empty?
    assert_redirected_to foundational_action_url(@foundational_action)
    @foundational_action.reload
    assert_equal name, @foundational_action.name
    assert_equal description, @foundational_action.description
  end

  test "foundational actions index when not logged in" do
    get foundational_actions_path
    assert_template 'index'
    assert_select 'div.pagination'
    first_page = FoundationalAction.paginate(page: 1)
    first_page.each do |foundational_action|
      assert_select 'a[href=?]', foundational_action_path(foundational_action),
                    text: foundational_action.name
      assert_select 'dd', foundational_action.description
      assert_select 'a[href=?]',
                    edit_foundational_action_path(foundational_action),
                    text: 'edit', count: 0
      assert_select 'a[href=?]', foundational_action_path(foundational_action),
                    text: 'delete', count: 0
    end
  end

  test "foundational actions index when editor" do
    log_in_as(@editor)
    get foundational_actions_path
    assert_template 'index'
    assert_select 'div.pagination'
    first_page = FoundationalAction.paginate(page: 1)
    first_page.each do |foundational_action|
      assert_select 'a[href=?]', foundational_action_path(foundational_action),
                    text: foundational_action.name
      assert_select 'dd', Regexp.new(foundational_action.description)
      assert_select 'a[href=?]',
                    edit_foundational_action_path(foundational_action),
                    text: 'edit'
      assert_select 'a[href=?]', foundational_action_path(foundational_action),
                    text: 'delete'
    end
    assert_difference 'FoundationalAction.count', -1 do
      delete foundational_action_path(@foundational_action)
    end
    assert_redirected_to foundational_actions_url
  end
end

