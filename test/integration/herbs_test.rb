require 'test_helper'

class HerbsTest < ActionDispatch::IntegrationTest

  def setup
    @editor = users(:archer)
    @herb = herbs(:dandelion)
  end

  test "invalid info for new herb" do
    log_in_as @editor
    get new_herb_path
    assert_no_difference 'Herb.count' do
      post herbs_path, herb: { name:  "",
                               latin: "" }
    end
    assert_template 'herbs/new'
  end
 
  test "create new herb with friendly forwarding" do
    get new_herb_path
    assert_redirected_to login_url
    follow_redirect!
    log_in_as(@editor)
    assert_redirected_to new_herb_url
    assert_difference 'Herb.count', 1 do
      post_via_redirect herbs_path, herb: { name: "lorem",
                                            latin: "ipsum",
                                            warming:    0,
                                            cooling:    1,
                                            drying:     2,
                                            moistening: 3,
                                            toning:     0,
                                            relaxing:   0 }
    end
    assert_template 'show'
  end

  test "unsuccessful edit" do
    log_in_as @editor
    get edit_herb_path(@herb)
    assert_template 'herbs/edit'
    patch herb_path(@herb), herb: { name:  "",
                                    latin: "" }
    assert_template 'herbs/edit'
  end

  test "successful edit with friendly forwarding" do
    get edit_herb_path(@herb)
    assert_redirected_to login_url
    follow_redirect!
    log_in_as @editor
    assert_redirected_to edit_herb_url(@herb)
    follow_redirect!
    assert_template 'herbs/edit'
    name = 'samwise'
    latin = 'agricola'
    patch herb_path(@herb), herb: { name: name, latin: latin }
    assert_redirected_to @herb
    assert_not flash.empty?
    @herb.reload
    assert_equal name, @herb.name
    assert_equal latin, @herb.latin
  end

  test "herbs index when not logged in" do
    get herbs_path
    assert_template 'herbs/index'
    assert_select 'div.pagination'
    first_page = Herb.paginate(page: 1)
    first_page.each do |herb|
      assert_select 'a[href=?]', herb_path(herb), text: herb.name.capitalize
      assert_select 'td', herb.latin.capitalize
      assert_select 'a[href=?]', edit_herb_path(herb), text: 'edit', count: 0
      assert_select 'a[href=?]', herb_path(herb), text: 'delete', count: 0
    end
  end

  test "herbs index when editor" do
    log_in_as(@editor)
    get herbs_path
    assert_template 'herbs/index'
    assert_select 'div.pagination'
    first_page = Herb.paginate(page: 1)
    first_page.each do |herb|
      assert_select 'a[href=?]', herb_path(herb), text: herb.name
      assert_select 'td', herb.latin.capitalize
      assert_select 'a[href=?]', edit_herb_path(herb), text: 'edit'
      assert_select 'a[href=?]', herb_path(herb), text: 'delete'
    end
    assert_difference 'Herb.count', -1 do
      delete herb_path(@herb)
    end
  end
end
