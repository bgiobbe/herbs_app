require 'test_helper'

class AffinitiesTest < ActionDispatch::IntegrationTest

  def setup
    @editor = users(:archer)
    @affinity = properties(:nervous)
  end

  test "invalid info for new property" do
    log_in_as(@editor)
    get new_affinity_path
    assert_no_difference 'Affinity.count' do
      post affinities_path, affinity: { name:  "",
                                        description: "lorem ipsum" }
    end
    assert_template 'new'
  end

  test "create new affinity with friendly forwarding" do
    get new_affinity_path
    assert_redirected_to login_url
    follow_redirect!
    log_in_as(@editor)
    assert_redirected_to new_affinity_url
    assert_difference 'Affinity.count', 1 do
      post_via_redirect affinities_path,
                        affinity: { name:  "body part",
                                    description: "lorem ipsum" }
    end
    assert_template 'show'
  end

  test "unsuccessful edit" do
    log_in_as(@editor)
    get edit_affinity_path(@affinity)
    assert_template 'edit'
    patch affinity_path(@affinity),
          affinity: { name: "", description: "lorem ipsum" }
    assert_template 'edit'
  end

  test "successful edit with friendly forwarding" do
    get edit_affinity_path(@affinity)
    assert_redirected_to login_url
    follow_redirect!
    log_in_as(@editor)
    assert_redirected_to edit_affinity_url(@affinity)
    follow_redirect!
    assert_template 'edit'
    name = "xyzzy"
    description = "lorem ipsum"
    patch affinity_path(@affinity),
          affinity: { name: name, description: description }
    assert_not flash.empty?
    assert_redirected_to affinity_url(@affinity)
    @affinity.reload
    assert_equal name, @affinity.name
    assert_equal description, @affinity.description
  end

  test "affinities index when not logged in" do
    get affinities_path
    assert_template 'index'
    assert_select 'div.pagination'
    first_page = Affinity.paginate(page: 1)
    first_page.each do |affinity|
      assert_select 'a[href=?]', affinity_path(affinity),
                    text: affinity.name
      assert_select 'dd', affinity.description
      assert_select 'a[href=?]', edit_affinity_path(affinity),
                    text: 'edit', count: 0
      assert_select 'a[href=?]', affinity_path(affinity),
                    text: 'delete', count: 0
    end
  end

  test "affinities index when editor" do
    log_in_as(@editor)
    get affinities_path
    assert_template 'index'
    assert_select 'div.pagination'
    first_page = Affinity.paginate(page: 1)
    first_page.each do |affinity|
      assert_select 'a[href=?]', affinity_path(affinity),
                    text: affinity.name
      assert_select 'dd', Regexp.new(affinity.description)
      assert_select 'a[href=?]',
                    edit_affinity_path(affinity),
                    text: 'edit'
      assert_select 'a[href=?]', affinity_path(affinity),
                    text: 'delete'
    end
    assert_difference 'Affinity.count', -1 do
      delete affinity_path(@affinity)
    end
    assert_redirected_to affinities_url
  end
end

