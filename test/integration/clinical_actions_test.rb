require 'test_helper'

class ClinicalActionsTest < ActionDispatch::IntegrationTest

  def setup
    @editor = users(:archer)
    @clinical_action = properties(:analgesic)
  end

  test "invalid info for new property" do
    log_in_as(@editor)
    get new_clinical_action_path
    assert_no_difference 'ClinicalAction.count' do
      post clinical_actions_path, clinical_action: {
                                        name:  "",
                                        description: "lorem ipsum" }
    end
    assert_template 'new'
  end

  test "create new clinical action with friendly forwarding" do
    get new_clinical_action_path
    assert_redirected_to login_url
    follow_redirect!
    log_in_as(@editor)
    assert_redirected_to new_clinical_action_url
    assert_difference 'ClinicalAction.count', 1 do
      post_via_redirect clinical_actions_path,
                        clinical_action: { name:  "action",
                                               description: "lorem ipsum" }
    end
    assert_template 'show'
  end

  test "unsuccessful edit" do
    log_in_as(@editor)
    get edit_clinical_action_path(@clinical_action)
    assert_template 'edit'
    patch clinical_action_path(@clinical_action),
          clinical_action: { name: "", description: "lorem ipsum" }
    assert_template 'edit'
  end

  test "successful edit with friendly forwarding" do
    get edit_clinical_action_path(@clinical_action)
    assert_redirected_to login_url
    follow_redirect!
    log_in_as(@editor)
    assert_redirected_to edit_clinical_action_url(@clinical_action)
    follow_redirect!
    assert_template 'edit'
    name = "xyzzy"
    description = "lorem ipsum"
    patch clinical_action_path(@clinical_action),
          clinical_action: { name: name,
                                 description: description }
    assert_not flash.empty?
    assert_redirected_to clinical_action_url(@clinical_action)
    @clinical_action.reload
    assert_equal name, @clinical_action.name
    assert_equal description, @clinical_action.description
  end

  test "clinical actions index when not logged in" do
    get clinical_actions_path
    assert_template 'index'
    assert_select 'div.pagination'
    first_page = ClinicalAction.paginate(page: 1)
    first_page.each do |clinical_action|
      assert_select 'a[href=?]', clinical_action_path(clinical_action),
                    text: clinical_action.name
      assert_select 'dd', clinical_action.description
      assert_select 'a[href=?]',
                    edit_clinical_action_path(clinical_action),
                    text: 'edit', count: 0
      assert_select 'a[href=?]', clinical_action_path(clinical_action),
                    text: 'delete', count: 0
    end
  end

  test "clinical actions index when editor" do
    log_in_as(@editor)
    get clinical_actions_path
    assert_template 'index'
    assert_select 'div.pagination'
    first_page = ClinicalAction.paginate(page: 1)
    first_page.each do |clinical_action|
      assert_select 'a[href=?]', clinical_action_path(clinical_action),
                    text: clinical_action.name
      assert_select 'dd', Regexp.new(clinical_action.description)
      assert_select 'a[href=?]',
                    edit_clinical_action_path(clinical_action),
                    text: 'edit'
      assert_select 'a[href=?]', clinical_action_path(clinical_action),
                    text: 'delete'
    end
    assert_difference 'ClinicalAction.count', -1 do
      delete clinical_action_path(@clinical_action)
    end
    assert_redirected_to clinical_actions_url
  end
end

