class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.string :name
      t.text   :description
      t.string :type

      t.timestamps null: false
    end
    add_index :properties, :name, unique: true
  end
end
