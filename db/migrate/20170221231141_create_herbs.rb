class CreateHerbs < ActiveRecord::Migration
  def change
    create_table :herbs do |t|
      t.string :name, null: false, index: true, unique: true
      t.string :latin
      t.integer :warming, limit: 1, default: 0
      t.integer :cooling, limit: 1, default: 0
      t.integer :drying, limit: 1, default: 0
      t.integer :moistening, limit: 1, default: 0
      t.integer :toning, limit: 1, default: 0
      t.integer :relaxing, limit: 1, default: 0

      t.timestamps null: false
    end
  end
end
