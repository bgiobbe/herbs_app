# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
User.create!(name:  "Beth Giobbe",
             email: "bgiobbe@gmail.com",
             password:              "xyzzyy",
             password_confirmation: "xyzzyy",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

## Create fake users, e.g. to test pagination on users index page
#99.times do |n|
#  name  = Faker::Name.name
#  email = "example-#{n+1}@railstutorial.org"
#  password = "password"
#  User.create!(name:  name,
#               email: email,
#               password:              password,
#               password_confirmation: password,
#               activated: true,
#               activated_at: Time.zone.now)
#end

Herb.create!(name: "Dandelion",
             latin: "Taraxacum officianale",
             warming:    0,
             cooling:    2,
             drying:     2,
             moistening: 0,
             toning:     0,
             relaxing:   0)
Herb.create!(name: "Peppermint",
             latin: "Mentha piperita",
             warming:    0,
             cooling:    2,
             drying:     2,
             moistening: 0,
             toning:     0,
             relaxing:   0)

# Copied properties from
# https://quizlet.com/44302439/foundational-primary-herbal-actions-flash-cards/

Property.create!(name: "adaptogen",
                 description: "increase the ability of the body to cope with " \
                              "and respond to stress",
                 type: "FoundationalAction")
Property.create!(name: "alterative",
                 description: "coordinate and improve the efficacy of our " \
                              "metabolism]",
                 type: "FoundationalAction")
Property.create!(name: "aromatic",
                 description: "contain volatile oils which are anti-" \
                              "inflammatory, anti-microbial, and are " \
                              "\"dispersive\" in nature, which is to say that " \
                              "they help break up stagnation of all sorts.",
                 type: "FoundationalAction")
Property.create!(name: "astringent",
                 description: "cause tissue to contract, and so are indicated" \
                              " when tissues are weak, atonic and/or " \
                              "\"leaking\"",
                 type: "FoundationalAction")
Property.create!(name: "Demulcent/Emollient/Mucilaginous",
                 description: "lubricate tissues, ease dryness, and soothe " \
                              "inflammation, irritation and injury.",
                 type: "FoundationalAction")
Property.create!(name: "trophorestorative",
                 description: "herbs whose use results not only in restored " \
                              "structure (as in astringents) but in restored " \
                              "function as well. Will create lasting " \
                              "improvement in structure in function that " \
                              "persists even if the herb itself is " \
                              "discontinued.",
                 type: "FoundationalAction")
Property.create!(name: "tonic",
                 description: "usually an herb or food that acts on the body " \
                              "in a slow, nutritive fashion to build up the " \
                              "substance of the body. It can also be defined " \
                              "as a substance which restrains loss from the " \
                              "body by \"toning\" tissues.",
                 type: "FoundationalAction")
Property.create!(name: "bitter tonics",
                 description: "used to strengthen and nourish the liver and " \
                              "metabolism (alteratives, for the most part)",
                 type: "FoundationalAction")
Property.create!(name: "sweet tonics",
                 description: "act primarily on the immune system and " \
                              "adrenals (adaptogens)",
                 type: "FoundationalAction")
Property.create!(name: "oily tonics",
                 description: "supply fixed oils and essential fatty acids " \
                              "to tissues to ensure hydration, cell " \
                              "permeability and to prevent atrophy.",
                 type: "FoundationalAction")
Property.create!(name: "mineral tonics",
                 description: "provide essential minerals",
                 type: "FoundationalAction")
Property.create!(name: "sour tonics",
                 description: "rich in bioflavinoids",
                 type: "FoundationalAction")
Property.create!(name: "bitter herbs",
                 description: "stimulate the secretion of of pretty much all " \
                              "the digestive acids, juices and enzymes, which " \
                              "generally improves appetite & digestion, " \
                              "especially of fats/oils/lipids. They also " \
                              "increase absorption of nutrients by supporting " \
                              "the processes that breakdown and absorb " \
                              "nutrients",
                 type: "FoundationalAction")
Property.create!(name: "Diaphoretic",
                 description: "used to open the pores and promote " \
                              "perspiration. They are usually used in the " \
                              "treatment of fevers, but can be used to " \
                              "advantage in colds & flus as well. They " \
                              "act on the hypothalamus, which controls " \
                              "the body's thermostat by directing the actions " \
                              "of peripheral circulation and the pores.",
                 type: "FoundationalAction")
Property.create!(name: "sudorific",
                 description: "Causes or increases perspiration",
                 type: "FoundationalAction")
Property.create!(name: "diuretic",
                 description: "increases the quantity of urine expelled from " \
                              "the body. Some do this by increasing the blood " \
                              "flow to the kidneys, others affect the " \
                              "secretion or reabsorbtion of fluids in the " \
                              "kidneys, while still others irritate the renal " \
                              "tissues and the kidneys produce more urine to " \
                              "try to \"flush out\" the irritant",
                 type: "FoundationalAction")
Property.create!(name: "relaxant",
                 description: "It does not refer to herbs that are sedative, " \
                              "but rather herbs that relax contracted tissues, " \
                              "such as antispasmodics.",
                 type: "FoundationalAction")
Property.create!(name: "Nervine/Sedative/Hypnotic/Soporific",
                 description: "act on the nervous system and there are different " \
                 "kinds",
                 type: "FoundationalAction")
Property.create!(name: "stimulant",
                 description: "increases activity of any sort of tissue or " \
                              "process... sialagogues are an example",
                 type: "FoundationalAction") 

# Create fake properties, e.g. to test pagination on index page
#names = Faker::Lorem.words(200, true).uniq
#byebug # make sure names has at least 150 elements
#names.slice(0,50).each do |name|
#  description = Faker::Lorem.sentence
#  FoundationalAction.create(name:  name, description: description)
#end
#
#names.slice(50,50).each do |name|
#  name  = Faker::Lorem.word
#  description = Faker::Lorem.sentence
#  ClinicalAction.create(name:  name, description: description)
#end
#
#names.slice(100,50).each do |name|
#  name  = Faker::Lorem.word
#  description = Faker::Lorem.sentence
#  Affinity.create(name:  name, description: description)
#end

