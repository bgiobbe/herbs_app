# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170328012220) do

  create_table "herbs", force: :cascade do |t|
    t.string   "name",                             null: false
    t.string   "latin"
    t.integer  "warming",    limit: 1, default: 0
    t.integer  "cooling",    limit: 1, default: 0
    t.integer  "drying",     limit: 1, default: 0
    t.integer  "moistening", limit: 1, default: 0
    t.integer  "toning",     limit: 1, default: 0
    t.integer  "relaxing",   limit: 1, default: 0
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "herbs", ["name"], name: "index_herbs_on_name"

  create_table "properties", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "type"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "properties", ["name"], name: "index_properties_on_name", unique: true

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",             default: false
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.boolean  "editor",            default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
