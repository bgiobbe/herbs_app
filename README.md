# Herbs Database

When I took the apprenticeship class at CommonWealth Center for Holistic 
Herbalism in 2014, the early lesson on energetics, organ affinities, and
foundational actions provoked in me a vision of a searchable database. Most
information about herbs on the web is in the form of long pages of text,
maybe with a photo. Any common search engine allows searching for such
information by the name of the herb, or for lists of herbs good for 
some given condition, but there is presently no way to search for herbs
by their properties. This application attempts to provide that service.


This application is based on the sample application built in 
[*Ruby on Rails Tutorial: Learn Web Development with Rails (3rd ed.)*]
(http://3rd-edition.railstutorial.org/) complete through Chapter 10.
Thanks go to Michael Hartl for the Users model with professional-
grade authentication and authorization logic.

